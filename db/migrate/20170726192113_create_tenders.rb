class CreateTenders < ActiveRecord::Migration
  def change
    create_table :tenders do |t|
      t.string :title
      t.string :file

      t.timestamps null: false
    end
  end
end
