class AddMobileToComplaints < ActiveRecord::Migration
  def change
    add_column :complaints, :mobile, :string
  end
end
