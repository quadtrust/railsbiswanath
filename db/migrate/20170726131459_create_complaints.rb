class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.string :sender
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
