# == Schema Information
#
# Table name: tenders
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  file       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tender < ActiveRecord::Base
	mount_uploader :file, FileUploader 
	validates :title, :file, presence: true
end
