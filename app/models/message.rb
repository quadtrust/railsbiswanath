# == Schema Information
#
# Table name: messages
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  email       :string(255)
#  mobile      :string(255)
#  title       :string(255)
#  description :text(65535)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Message < ActiveRecord::Base
	validates :name, :email, :mobile, :title, :description, presence: true
end
