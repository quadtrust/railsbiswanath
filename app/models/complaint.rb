# == Schema Information
#
# Table name: complaints
#
#  id          :integer          not null, primary key
#  sender      :string(255)
#  title       :string(255)
#  description :text(65535)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  email       :string(255)
#  mobile      :string(255)
#

class Complaint < ActiveRecord::Base
	validates :sender, :email, :mobile, :title, :description, presence: true
end
