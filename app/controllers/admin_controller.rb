class AdminController < ApplicationController 
	before_action :authenticate_user!
	def index 
		@complaints = Complaint.all
		@messages = Message.all
	end
end