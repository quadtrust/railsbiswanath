class ComplaintsController < ApplicationController 
	def create
	@complaint = Complaint.new(complaints_params)
    respond_to do |format|
      if @complaint.save
        format.html { redirect_to root_url, notice: 'News was successfully created.' }
      else
        format.html { redirect_to :complaints_path }
      end
    end
	end

	def destroy
		@complaint = Complaint.find(params[:id])
		@complaint.destroy
		respond_to do |format|
         format.html { redirect_to admin_url, notice: 'News was successfully destroyed.' }
         format.json { head :no_content }
        end
	end

	private 

	def complaints_params 
		params.require(:complaint).permit(:title, :sender, :description, :email, :mobile)
	end
end