class MessagesController < ApplicationController 
	before_action :authenticate_user!, only: [:destroy]
	def new
 		@message = Message.new
 	end
 	def create
	@message = Message.new(messages_params)
    respond_to do |format|
      if @message.save
        format.html { redirect_to root_url, notice: 'News was successfully created.' }
      else
        format.html { redirect_to :root_path }
      end
    end
	end

	def destroy
		@message = Message.find(params[:id])
		@message.destroy
		respond_to do |format|
         format.html { redirect_to admin_url, notice: 'News was successfully destroyed.' }
         format.json { head :no_content }
        end
	end

	private 

	def messages_params 
		params.require(:message).permit(:title, :name, :mobile, :description)
	end
end