 class TendersController < ApplicationController 

 	before_action :authenticate_user!

 	def new
 		@tender = Tender.new
 	end
 	def create
	@tender = Tender.new(tenders_params)
    respond_to do |format|
      if @tender.save
        format.html { redirect_to tender_and_notice_url, notice: 'News was successfully created.' }
      else
        format.html { redirect_to :projects_path }
      end
    end
	end

	def destroy
		@tender = Tender.find(params[:id])
		@tender.destroy
		respond_to do |format|
         format.html { redirect_to tender_and_notice_url, notice: 'News was successfully destroyed.' }
         format.json { head :no_content }
        end
	end

	private 

	def tenders_params 
		params.require(:tender).permit(:title, :file)
	end
 end