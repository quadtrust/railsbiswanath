Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   authenticated :user do
    root 'admin#index', as: :root
  end

  unauthenticated :user do
    root 'home#index', as: :unauthenticated_root
  end

   get 'contact' => 'home#contact_us'
   get 'complaints' => 'home#complaints'
   get 'admin' => 'admin#index'
   get 'committee_members' => 'home#committee_members'
   get 'message_chairperson' => 'home#message_chairperson'
   get 'message_executive' => 'home#message_executive'
   get 'ongoing_projects' => 'home#ongoing_projects'
   get 'future_projects' => 'home#future_projects'
   get 'about_board' => 'home#about_board'
   get 'tender_and_notice' => 'home#tender_and_notice'
   get 'special_member' => 'home#special_member'
   get 'services' => 'home#services'
   get 'assets' => 'home#assets'
   get 'past_bearers' => 'home#past_bearers'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
    resources :complaints
    resources :tenders
    resources :messages

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
